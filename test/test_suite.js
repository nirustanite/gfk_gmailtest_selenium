const dotenv = require('dotenv').config();
const driverConfig = require('./driverConfig');
const login = require('./login');
const logout = require('./logout');
const promise = require('bluebird');
const config = require('./config'); 
const composemail =  require('./compose');
const verification = require('./verification');

describe("GMAIL TEST",function(){
     
     // driver should be loaded before each testcase
     beforeEach(function(){

         let configdriver = driverConfig.getConfig();
         driver = configdriver.driver;
         Webdriver = configdriver.Webdriver
     });


     afterEach(function(){
       driver.quit();
   
     });

     it('Test1 - Compose mail',async function(){
     	 this.timeout(70000);
        try{
        await login(driver,Webdriver,config.senderEmail,config.senderPassword);
        await composemail(driver,Webdriver,config.recipientEmail);
        await logout(driver,Webdriver);
        return promise.resolve();
        }catch(e){
            return promise.reject(e);
        }
     });

    it('Test2 - Verification',async function(){
         this.timeout(70000);
         try
         {
            await login(driver,Webdriver,config.recipientEmail,config.recipientPassword);
            await verification(driver,Webdriver);
            await logout(driver,Webdriver);
            return promise.resolve();
         }
         catch(e)
         {
            return promise.reject(e);
         }
     });

});