const config = require('./config');
const promise = require('bluebird');
async function login(driver,Webdriver,Email,Password)
{
	await promise.delay(2000);
	await driver.get(config.gmailHost);
	await driver.findElement(Webdriver.By.id('identifierId')).sendKeys(Email);
    await promise.delay(1000);
    await driver.findElement(Webdriver.By.id('identifierNext')).click();
    await promise.delay(2000);
    await driver.findElement(Webdriver.By.name('password')).sendKeys(Password);
    await promise.delay(1000);
    await driver.findElement(Webdriver.By.id('passwordNext')).click();
    await promise.delay(5000);
}
    

module.exports = login;