// building chrome driver
const Webdriver = require('selenium-webdriver');
module.exports = {
	getConfig: function(){
		var chromeCapabilities = Webdriver.Capabilities.chrome();
		var chromeOptions = {'args': ['--test-type', '--incognito', '--disable-extensions']};
                chromeCapabilities.set('chromeOptions', chromeOptions);
                let driver = new Webdriver.Builder().withCapabilities(chromeCapabilities).build();
                driver.manage().window().maximize();
		return {Webdriver, driver};
	}
};