module.exports = {
	gmailHost: process.env.GMAIL_HOST,
	senderEmail: process.env.SENDER_EMAIL,
	senderPassword: process.env.SENDER_PASSWORD,
	recipientEmail: process.env.RECIPIENT_EMAIL,
	recipientPassword: process.env.RECIPIENT_PASSWORD,
	mailSubject: process.env.MAIL_SUBJECT,
	mailBody: process.env.MAIL_BODY,
	randomSubject: (new Date()).getTime()
}