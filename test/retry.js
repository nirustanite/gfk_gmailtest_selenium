
const promise = require('bluebird');
async function retryAction(Webdriver,driver,byType, byValue, action){
	let retry = 1;
	while(retry <= 30)
	{
		try{
            await promise.delay(1000);
			await driver.findElement(Webdriver.By[byType](byValue))[action]();
			break;
		}
		catch(e)
		{
			retry ++;
			continue;
		}
	}
}

module.exports = retryAction;

