const config = require('./config');
const promise = require('bluebird');
const chai = require('chai');
async function verification(driver,Webdriver)
{

	await driver.findElement(Webdriver.By.xpath("//*[@class='bog']/span")).click();
	await promise.delay(1000);
    let mailsubject = await driver.findElement(Webdriver.By.xpath("//*[@class='hP']")).getText();
    chai.expect(mailsubject).to.deep.equal(`${config.mailSubject} - ${config.randomSubject}`);
    let mailbody = await driver.findElement(Webdriver.By.xpath("//*[@class='a3s aXjCH ']/div[1]")).getText();
    chai.expect(mailbody).to.deep.equal(`${config.mailBody}`);
}

module.exports = verification;