const config = require('./config');
const promise = require('bluebird');
async function logout(driver,Webdriver)
{
	await promise.delay(1000);
	await driver.findElement(Webdriver.By.xpath("//*[@class='gb_9a gbii']")).click();
    await promise.delay(2000);
    await driver.findElement(Webdriver.By.xpath("//*[contains(text(),'Sign out')]")).click();
    await promise.delay(1000);
}

module.exports = logout;