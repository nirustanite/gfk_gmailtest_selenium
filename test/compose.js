const config = require('./config');
const driverConfig = require('./driverConfig');
const promise = require('bluebird');
const chai = require('chai');
const retry = require('./retry');

//Actions action = new Actions();
async function composemail(driver,Webdriver,recipientEmail)
{	


	await retry(Webdriver,driver,"xpath","//*[@class='T-I J-J5-Ji T-I-KE L3']","click");
	await promise.delay(5000);
	await driver.findElement(Webdriver.By.name('to')).clear();
	await driver.findElement(Webdriver.By.name('to')).sendKeys(recipientEmail);
	await promise.delay(1000);
	if(driver.findElement(Webdriver.By.xpath(`//*[@data-hovercard-id='${recipientEmail}']`)))
	{
		await driver.findElement(Webdriver.By.xpath(`//*[@data-hovercard-id='${recipientEmail}']`)).click();
		await promise.delay(1000);
	}

	await driver.findElement(Webdriver.By.name('subjectbox')).sendKeys(`${config.mailSubject} - ${config.randomSubject}`);
	await promise.delay(1000);
	await driver.findElement(Webdriver.By.xpath("//div[@aria-label='Message Body']")).sendKeys(config.mailBody);
	await promise.delay(1000);
	await driver.findElement(Webdriver.By.xpath("//*[@role='button' and (.)='Send']")).click();
    await promise.delay(1000);
	await retry(Webdriver,driver,"xpath","//*[contains(text(),'Sent')]","click");
    await promise.delay(2000);

}

module.exports = composemail;