### GMAIL Automation Testing 
### Using Selenium Webdriver in Javascript

**TASK**

1. Login to Gmail.
2. Compose a mail.
3. Verify whether mail is sent.
4. Sign out.
5. Sign In to other account.
6. Check whether the mail is received.

---

** PARAMETERS REQUIRED **

1. URL - "http://www.gmail.com".
2. Sender Email Address.
3. Sender Password.
4. Recipient Email Address.
5. Recipient Password.
6. Subject.
7. Body.

----

**COMMAND TO RUN THE PROGRAM**

npm run test

---

**PREREQUISTES**

1. Change .env.sample to .env
2. Enter the values to unfilled properties.

**ASSUMPTIONS MADE**

1. Install chrome driver. The program will run on Chrome only.
2. Works with regular Gmail without 2 step verification.
